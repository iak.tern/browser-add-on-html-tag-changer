# Browser Add-on - HTML Tag Changer

This browser add-on can change the HTML element tag names on a web page into another tag name. You can select the HTML elements that should be replaced with a CSS selector and state the new tag names.

Furthermore you can hide or delete the selected elements from the web page. The former is usually preferred, because you will be able to revert the changes afterwards. If you use delete, you need to reload the complete page to revert the changes.

**Example:** The Reader View of Firefox includes and presents only a selection of HTML elements like `<p>`. But if the web page contains important hint or content boxes in `<aside>` elements, you will not see them in the Reader View. You can use this add-on to change all `<aside>` elements in appropriate elements before you turn on the Reader. 

There is a _Revert_ button - if it doesn't work correctly, just reload the complete page.

![Add-On picture filled with example entries](add-on_pictures/filled_view.png)

## Hints:

- Due to a lack of API support, the event listeners for an element can not be copied into the new element
- Pay attention to the structure of a web page: some elements (often inline elements) don't allow to include other elements. E.g. if you want to change all `<aside>` elements into `<p>` elements, this can lead to unpredictable behavior if the `<aside>` element contains further block elements, because `<p>` is not allowed to contain block elements.
- If the replacement contains web components, this can also lead to some side effects

## Details:

The add-on does not delete the original elements, but it hides them. Therefore the actions can be reverted. 

Original Code:
![original code](add-on_pictures/code_original.png)

After the add-on transformed the code
![original replaced](add-on_pictures/code_replaced.png)
