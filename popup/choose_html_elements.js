/**
 * CSS to hide everything on the page,
 * except for elements that have the "beastify-image" class.
 */
const hidePage = `body > :not(.beastify-image) {
    display: none;
  }`;

/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function listenForClicks() {
  document.addEventListener('click', (e) => {
    /**
     * Insert the page-hiding CSS into the active tab,
     * then get the beast URL and
     * send a "beastify" message to the content script in the active tab.
     */
    function sendElements(tabs) {
      browser.tabs.sendMessage(tabs[0].id, {
        command: 'change',
        // rename
        htmlElementsSelector: getElementSelector(),
        replaceWithTag: getNewTagName(),
      });
    }

    function getElementSelector() {
      return document.getElementById('elementsToBeReplaced').value.trim();
    }

    function getNewTagName() {
      return document.getElementById('replaceWith').value.trim();
    }
    /**
     * Remove the page-hiding CSS from the active tab,
     * send a "reset" message to the content script in the active tab.
     */
    function reset(tabs) {
      browser.tabs.sendMessage(tabs[0].id, {
        command: 'reset',
      });
    }

    function hide(tabs) {
      browser.tabs.sendMessage(tabs[0].id, {
        command: 'change',
        // rename
        htmlElementsSelector: getElementSelector(),
        replaceWithTag: null,
      });
    }

    function deleteElements(tabs) {
      browser.tabs.sendMessage(tabs[0].id, {
        command: 'delete',
        // rename
        htmlElementsSelector: getElementSelector(),
        replaceWithTag: null,
      });
    }

    /**
     * Just log the error to the console.
     */
    function reportError(error) {
      console.error(`Could not change elements: ${error}`);
    }

    /**
     * Get the active tab,
     * then call "beastify()" or "reset()" as appropriate.
     */
    if (e.target.classList.contains('transform')) {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then(sendElements)
        .catch(reportError);
    } else if (e.target.classList.contains('revert')) {
      browser.tabs.query({ active: true, currentWindow: true }).then(reset).catch(reportError);
    } else if (e.target.classList.contains('hide')) {
      browser.tabs.query({ active: true, currentWindow: true }).then(hide).catch(reportError);
    } else if (e.target.classList.contains('delete')) {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then(deleteElements)
        .catch(reportError);
    }
  });
}

/**
 * There was an error executing the script.
 * Display the popup's error message, and hide the normal UI.
 */
function reportExecuteScriptError(error) {
  document.querySelector('#popup-content').classList.add('hidden');
  document.querySelector('#error-content').classList.remove('hidden');
  console.error(`Failed to execute beastify content script: ${error.message}`);
}

/**
 * When the popup loads, inject a content script into the active tab,
 * and add a click handler.
 * If we couldn't inject the script, handle the error.
 */
browser.tabs
  .executeScript({ file: '/content_scripts/change_html.js' })
  .then(listenForClicks)
  .catch(reportExecuteScriptError);
