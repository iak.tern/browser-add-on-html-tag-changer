module.exports = {
  ignoreFiles: ['.directory', '.git', 'add-on_pictures', 'web-ext-artifacts', 'web-ext-config.js'],
  build: {
    overwriteDest: true,
  },
};
