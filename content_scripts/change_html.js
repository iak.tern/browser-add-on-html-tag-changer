/* 
HTML Tag Changer add-on to change the tag name of a HTML element
Copyright (C) 2020 Kai Grunert

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

(function () {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  if (window.hasChangeScript) {
    return;
  }
  window.hasChangeScript = true;

  let timerId;

  /**
   * Looks for all target elements and replaces them
   */
  function changeElements(elementToBeReplaced, replacedWith) {
    console.debug(
      `HTML Tag Changer: find and loop over all elements: '${elementToBeReplaced}' to be '${replacedWith}'`,
    );
    const newTagName = replacedWith.trim();
    const myNodeList = document.querySelectorAll(elementToBeReplaced);

    for (let node of myNodeList) {
      // console.debug(node.outerHTML.substring(0, 30));

      let originalTagName = node.tagName.toLowerCase();

      // RegEx: ^ start with, e.g. start with "<aside", i = ignore case
      const replaceRegExStart = new RegExp('^<' + originalTagName, 'i');
      const replaceRegExEnd = new RegExp(originalTagName + '>$', 'i');
      let newHTML = node.outerHTML
        .replace(replaceRegExStart, '<' + newTagName)
        .replace(replaceRegExEnd, newTagName + '>');

      // Create Node/Element from String: https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro
      let tmpTemplateElement = document.createElement('template');
      tmpTemplateElement.innerHTML = newHTML;
      //https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore
      let copyWithNewTag = node.parentNode.insertBefore(
        tmpTemplateElement.content.firstElementChild.cloneNode(true),
        node,
      );
      // Alternative: node.insertAdjacentHTML('beforebegin', newHTML);
      // Problem: also allows wrong html and transforms it unpredictably
      // e.g. if new <p> contains block elements
      // console.debug(node.previousSibling.outerHTML.substring(0, 30));

      //For reverting: add some easy to find class
      copyWithNewTag.classList.add('html-tag-changer-replacement');
      node.classList.add('html-tag-changer-original');

      node.hidden = true;

      console.debug(
        `HTML Tag Changer: element ${node.outerHTML
          .substring(0, 30)
          .replace(/\r?\n|\r/g, ' ')} ...>  replaced`,
      );
    }
    console.debug(`HTML Tag Changer: All "${elementToBeReplaced}" elements have been replaced.`);

    // wait with the verification to let dynamic bahavior execute
    // e.g. HTTP GET for CSS files
    timerId = setTimeout(verifyNewElements, 2000);
  }

  /**
   * This functions compares the original with the replacement.
   * If there are too many elements in the replacement (which can
   * sometimes happen in web components or because of dynamic
   * additions), it deletes the elements.
   * If both Elements do not contain the same elements (which can
   * happen with inline elements) an error is thrown to the console.
   *
   * wenn child-liste kleiner, dann löschen, weil was schief gegangen ist
   * wenn gleich oder größer, durchsuchen und die überflüssigen Elemente löschen
   */
  function verifyNewElements() {
    console.debug('HTML Tag Changer: start replacement verification');

    let replacedElements = document.querySelectorAll('.html-tag-changer-replacement');
    for (let replaced of replacedElements) {
      let original = replaced.nextElementSibling;

      // if the next sibling element is not the original something went wrong
      // delete it until the original comes
      // => probably not needed anymore, because with the (new) <template> method
      // the original problem is not there anymore -> maybe I find another problem later
      //
      // while (!replaced.nextElementSibling.classList.contains('html-tag-changer-original')) {
      //   console.error(
      //     'HTML Tag Changer: something went wrong. Clean up = delete element: ' +
      //       replaced.nextElementSibling.substring(0, 15),
      //   );
      //   replaced.nextElementSibling.remove();
      // }

      if (!original.classList.contains('html-tag-changer-original')) {
        console.error(
          'HTML Tag Changer: something went wrong. Please inform the add-on developer about your szenario.',
        );
        return;
      }

      if (replaced.children.length < original.children.length) {
        console.warn(
          `HTML Tag Changer: Your replacement resulted in an error. You have probably violated the HTML rules. (E.g. a <p> is not allowed to contain block elements like <div>.). You have tried to replace ${original.outerHTML
            .substring(0, 30)
            .replace(/\r?\n|\r/g, ' ')} ...> with <${replaced.tagName.toLowerCase()}>.`,
        );
        replaced.remove();
      }

      deepElementCompareAndClean(replaced, original);
    }
    console.debug('HTML Tag Changer: replacement verification done');
  }

  /**
   * Compares two HTML elements and their childs if they are the same.
   * It deletes every child that is in the first element (the replacement)
   * but not in the second.
   *
   * replaced.children has always >= original.children
   */
  function deepElementCompareAndClean(replaced, original) {
    let childsReplaced = replaced.children;
    let childsOriginal = original.children;

    for (let i = 0; i < childsReplaced.length; i++) {
      // case: too much because of dynamic addition
      // <img id=1>  <--->  <img id=1>
      // <div id=2>  <--->  <div id=2>
      // <img id=1>  <--->  nothing
      // <div id=2>  <--->  nothing
      if (childsOriginal[i] == undefined) {
        console.debug(
          `HTML Tag Changer: replacement has more children than original. 
          ${childsReplaced[i].outerHTML
            .substring(0, 30)
            .replace(/\r?\n|\r/g, ' ')} ...> will be removed`,
        );
        childsReplaced[i].remove();
        i--; //children is a live list, so now, it has one element less
        continue; // new loop start
      }

      if (htmlElementEquals(childsReplaced[i], childsOriginal[i]) == true) {
        // if the child also has children, then recursively go through it
        if (childsReplaced[i].children.length > 0) {
          deepElementCompareAndClean(childsReplaced[i], childsOriginal[i]);
        }

        // do nothing in good case
      } else {
        // elements not equal

        // case: too much because of dynamic addition
        // <img id=1>  <--->  <div id=1>
        // <div id=2>  <--->  <img id=2>
        // <img id=1>  <--->  nothing
        console.debug(
          `HTML Tag Changer: replacement has different child (${
            childsReplaced[i].tagName
          }) than original (${childsOriginal[i].tagName}). 
          ${childsReplaced[i].outerHTML
            .substring(0, 30)
            .replace(/\r?\n|\r/g, ' ')} ...> will be removed`,
        );
        childsReplaced[i].remove();
        i--; //children is a live list, so now, it has one element less
      }
    }
  }

  /**
   * Compares if two HTML elements are the "same" for this replacement use case.
   * Considers the tag name, id, name, className, attribute.length
   *
   * Does not consider the childrens, because this can be different and will be removed.
   *
   * @param {HTML Element} e1
   * @param {HTML Element} e2
   */
  function htmlElementEquals(e1, e2) {
    // console.log('Vergleiche Elemente');
    // console.log(`tagName: ${e1.tagName} == ${e2.tagName}
    // id: ${e1.id} == ${e2.id}
    // className: ${e1.className} == ${e2.className}
    // name: ${e1.name} == ${e2.name}
    // attributes: ${e1.attributes.length} == ${e2.attributes.length}
    // `);
    if (
      e1.tagName == e2.tagName &&
      e1.id == e2.id &&
      e1.className == e2.className &&
      e1.name == e2.name &&
      e1.attributes.length == e2.attributes.length
    ) {
      return true;
    }
    return false;
  }

  function removeSelectedElements(elementToBeReplaced) {
    console.debug(`HTML Tag Changer: find all elements for ${elementToBeReplaced} and delete them`);
    const myNodeList = document.querySelectorAll(elementToBeReplaced);

    for (let node of myNodeList) {
      node.remove();
      console.debug(
        `HTML Tag Changer: element ${node.outerHTML
          .substring(0, 60)
          .replace(/\r?\n|\r/g, ' ')} ...>  deleted`,
      );
    }
    console.debug('HTML Tag Changer: all elements deleted from the site');
  }

  function hideSelectedElements(elementToBeReplaced) {
    console.debug(`HTML Tag Changer: find all elements for ${elementToBeReplaced} to hide them`);
    const myNodeList = document.querySelectorAll(elementToBeReplaced);

    for (let node of myNodeList) {
      node.classList.add('html-tag-changer-original');
      node.hidden = true;
      console.debug(
        `HTML Tag Changer: element ${node.outerHTML
          .substring(0, 60)
          .replace(/\r?\n|\r/g, ' ')} ...>  hidden`,
      );
    }
    console.debug('HTML Tag Changer: all elements hidden from the site');
  }

  /**
   * Revert the changes
   */
  function removeReplacedElements() {
    //clear verification timer
    if (timerId) {
      clearTimeout(timerId);
    }
    let originalElements = document.querySelectorAll('.html-tag-changer-original');
    for (let original of originalElements) {
      original.classList.remove('html-tag-changer-original');
      original.hidden = false;
    }
    if (originalElements.length > 0) {
      console.debug('HTML Tag Changer: All prevously made changes have been reverted');
    }

    let replacedElements = document.querySelectorAll('.html-tag-changer-replacement');
    for (let element of replacedElements) {
      element.remove();
    }
    if (replacedElements.length == 0) {
      console.debug('HTML Tag Changer: No replacements have been removed');
    }
  }

  /**
   * Listen for messages from the background script.
   */
  browser.runtime.onMessage.addListener((message) => {
    if (message.command === 'change') {
      if (checkElementSectorEmpty(message.htmlElementsSelector)) {
        return;
      }
      if (!message.replaceWithTag) {
        hideSelectedElements(message.htmlElementsSelector);
        return;
      }
      if (message.replaceWithTag.split(' ').length > 1) {
        console.warn('HTML Tag Changer: element tag names are not allowed to have spaces.');
        return;
      }
      changeElements(message.htmlElementsSelector, message.replaceWithTag);
    } else if (message.command === 'reset') {
      removeReplacedElements();
    } else if (message.command === 'delete') {
      if (checkElementSectorEmpty(message.htmlElementsSelector)) {
        return;
      }
      removeSelectedElements(message.htmlElementsSelector);
    }
  });

  /**
   * Checks if the selector is not empty
   * @param {String} str
   * @returns {Boolean} true if it is empty
   */
  function checkElementSectorEmpty(str) {
    if (!str) {
      console.warn('HTML Tag Changer: no Element Selector given');
      return true;
    }
    return false;
  }
})();
